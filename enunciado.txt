1. Copia los dos directorios en un nuevo directorio fuera de éste (no copies el directorio .git).
2. En el nuevo directorio, crea un repositorio Git.
3. Añade al stage todos los archivos y crea un commit inicial con todos ellos.
4. Abre el archivo lampara/lampara.java y añade esta nueva propiedad a la clase. Guarda el archivo.

    private boolean tieneBombilla;

5. Abre el archivo lavadora/lavadora.java y haz que el método lava() sea privado. Guarda el archivo.
6. Añade los cambios al stage y haz un commit con ellos.
7. Abre el archivo lavadora/lavadora.java y corrige la visibilidad del método lava(), hazlo público. Guarda el archivo.
8. Abre el archivo lampara/lampara.java y añade este nuevo método. Guarda el archivo.

  public void switch() {
      estaEncendida = !estaEncendida;
  }
  
9. Añade únicamente la corrección del punto 7 al último commit que has hecho.
10. Crea un nuevo commit con el cambio del punto 8.
11. Comprueba que te ha quedado un log con tres commits.
12. Comprueba que en el penúltimo commit has corregido bien la visibilidad del método lava().
